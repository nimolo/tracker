console.log('tracker version 0.1');
let $46c182cf445ced10$var$running = false;
class $46c182cf445ced10$var$Project {
    constructor(){}
}
let $46c182cf445ced10$var$tracks = [];
let $46c182cf445ced10$var$chains = {};
let $46c182cf445ced10$var$phrases = {};
let $46c182cf445ced10$var$instruments = {};
let $46c182cf445ced10$var$pads = [];
let $46c182cf445ced10$var$samples = {};
let $46c182cf445ced10$var$fxs = [];
let $46c182cf445ced10$var$initialized = false;
let $46c182cf445ced10$var$synth = null;
let $46c182cf445ced10$var$synthA = null;
let $46c182cf445ced10$var$synthB = null;
let $46c182cf445ced10$var$masterReverb = null;
function $46c182cf445ced10$var$addInstrumentLFO(instrument, patch) {}
class $46c182cf445ced10$var$SampleInstrument {
    constructor(){
        this.instrument = new Tone.Player().toDestination();
    }
}
class $46c182cf445ced10$var$SynthInstrument {
    constructor(){
        this.instrument = new Tone.Synth().toDestination();
    }
}
let $46c182cf445ced10$var$initializer = document.querySelector('#initializer');
$46c182cf445ced10$var$initializer.addEventListener('click', $46c182cf445ced10$var$initApp);
function $46c182cf445ced10$var$initApp(event) {
    if (document.documentElement.requestFullscreen) {
        let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) document.documentElement.requestFullscreen();
        Tone.start().then(()=>{
            console.log('audio is ready');
            event.target.removeEventListener('click', $46c182cf445ced10$var$initApp);
            event.target.remove();
            document.querySelector('#song-editor').style.display = 'block';
        });
        if (!$46c182cf445ced10$var$initialized) {
            $46c182cf445ced10$var$initialized = true;
            $46c182cf445ced10$var$masterReverb = new Tone.Reverb({
                'decay': 2.0,
                'wet': 0.25
            }).toDestination();
            $46c182cf445ced10$var$synth = new Tone.Synth().chain($46c182cf445ced10$var$masterReverb); // .toDestination();
            let lfo = new Tone.LFO("4n", 400, 4000).start(0);
            $46c182cf445ced10$var$synthA = new Tone.FMSynth(); // .toDestination();
            $46c182cf445ced10$var$synthA.chain($46c182cf445ced10$var$masterReverb);
            const part = new Tone.Part((time, value)=>{
                // the value is an object which contains both the note and the velocity
                // console.log(value.instrument);
                let instrument = value.instrument;
                instrument.triggerAttackRelease(value.note, "8n", time, value.velocity);
            }, [
                {
                    time: 0,
                    note: "C3",
                    velocity: 0.9,
                    instrument: $46c182cf445ced10$var$synth
                },
                {
                    time: "1:0",
                    note: "C4",
                    velocity: 0.5,
                    instrument: $46c182cf445ced10$var$synth
                },
                {
                    time: "2:0",
                    note: "F4",
                    velocity: 0.5,
                    instrument: $46c182cf445ced10$var$synth
                },
                {
                    time: "3:0",
                    note: "G4",
                    velocity: 0.5,
                    instrument: $46c182cf445ced10$var$synth
                }, 
            ]).start(0);
            const loopA = new Tone.Loop((time)=>{
                $46c182cf445ced10$var$synthA.triggerAttackRelease("C2", "8n", time);
            }, "4n").start(0);
            let noiseSynth = new Tone.NoiseSynth({
                'volume': -9
            }).toDestination();
            const noiseLoop = new Tone.Loop((time)=>{
                noiseSynth.triggerAttackRelease("16n", time);
            }, "8n").start(0);
        }
    } else alert('no fullscreen');
}
function $46c182cf445ced10$var$startStop(event) {
    if ($46c182cf445ced10$var$running) {
        $46c182cf445ced10$var$running = false;
        Tone.Transport.stop();
    } else {
        $46c182cf445ced10$var$running = true;
        Tone.Transport.start();
    // synth.triggerAttackRelease("C4", "8n");
    }
}
function $46c182cf445ced10$var$triggerPad(event) {
    let pad = event.target.id;
    console.log(pad);
}
let $46c182cf445ced10$var$pad1 = document.querySelector('#pad1');
$46c182cf445ced10$var$pad1.addEventListener('click', $46c182cf445ced10$var$triggerPad);
let $46c182cf445ced10$var$pad13 = document.querySelector('#pad13');
$46c182cf445ced10$var$pad13.addEventListener('click', $46c182cf445ced10$var$startStop);


