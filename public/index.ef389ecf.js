console.log('tracker version 0.1');
let $46c182cf445ced10$var$running = false;
class $46c182cf445ced10$var$Project {
    constructor(){}
}
let $46c182cf445ced10$var$tracks = [];
let $46c182cf445ced10$var$chains = {};
let $46c182cf445ced10$var$phrases = {};
let $46c182cf445ced10$var$instruments = {};
let $46c182cf445ced10$var$pads = [];
let $46c182cf445ced10$var$samples = {};
let $46c182cf445ced10$var$fxs = [];
let $46c182cf445ced10$var$initialized = false;
let $46c182cf445ced10$var$synth = null;
let $46c182cf445ced10$var$synthA = null;
let $46c182cf445ced10$var$synthB = null;
let $46c182cf445ced10$var$masterReverb = null;
function $46c182cf445ced10$var$addInstrumentLFO(instrument, patch) {}
class $46c182cf445ced10$var$SampleInstrument {
    constructor(){
        this.instrument = new Tone.Player().toDestination();
    }
}
class $46c182cf445ced10$var$SynthInstrument {
    constructor(){
        this.instrument = new Tone.Synth().toDestination();
    }
}
let $46c182cf445ced10$var$initializer = document.querySelector('#initializer');
$46c182cf445ced10$var$initializer.addEventListener('click', $46c182cf445ced10$var$initApp);
function $46c182cf445ced10$var$moveMeasure() {}
function $46c182cf445ced10$var$moveNote() {}
function $46c182cf445ced10$var$initApp(event) {
    if (document.documentElement.requestFullscreen) {
        let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) document.documentElement.requestFullscreen();
        Tone.start().then(()=>{
            console.log('audio is ready');
            event.target.removeEventListener('click', $46c182cf445ced10$var$initApp);
            event.target.remove();
            document.querySelector('#song-editor').style.display = 'block';
        });
        if (!$46c182cf445ced10$var$initialized) {
            $46c182cf445ced10$var$initialized = true;
            Tone.Transport.scheduleRepeat((time)=>{
                // use the callback time to schedule events
                // osc.start(time).stop(time + 0.1);
                $46c182cf445ced10$var$moveNote();
            }, "8n");
            /*
            Tone.Transport.scheduleRepeat((time) => {
                moveMeasure();
            }, "1n");
            */ $46c182cf445ced10$var$masterReverb = new Tone.Reverb({
                'decay': 2.0,
                'wet': 0.25
            }).toDestination();
            $46c182cf445ced10$var$synth = new Tone.Synth().chain($46c182cf445ced10$var$masterReverb); // .toDestination();
            let lfo = new Tone.LFO("4n", 400, 4000).start(0);
            $46c182cf445ced10$var$synthA = new Tone.FMSynth(); // .toDestination();
            $46c182cf445ced10$var$synthA.chain($46c182cf445ced10$var$masterReverb);
            const part = new Tone.Part((time, value)=>{
                // the value is an object which contains both the note and the velocity
                // console.log(value.instrument);
                let instrument = value.instrument;
                instrument.triggerAttackRelease(value.note, "8n", time, value.velocity);
            }, [
                {
                    time: 0,
                    note: "C3",
                    velocity: 0.9,
                    instrument: $46c182cf445ced10$var$synth
                },
                {
                    time: "1:0",
                    note: "C4",
                    velocity: 0.5,
                    instrument: $46c182cf445ced10$var$synth
                },
                {
                    time: "2:0",
                    note: "F4",
                    velocity: 0.5,
                    instrument: $46c182cf445ced10$var$synth
                },
                {
                    time: "3:0",
                    note: "G4",
                    velocity: 0.5,
                    instrument: $46c182cf445ced10$var$synth
                }, 
            ]).start(0);
            const loopA = new Tone.Loop((time)=>{
                $46c182cf445ced10$var$synthA.triggerAttackRelease("C2", "8n", time);
            }, "4n").start(0);
            let noiseSynth = new Tone.NoiseSynth({
                'volume': -9
            }).toDestination();
            const noiseLoop = new Tone.Loop((time)=>{
                noiseSynth.triggerAttackRelease("16n", time);
            }, "8n").start(0);
        }
    } else alert('no fullscreen');
}
function $46c182cf445ced10$var$startStop(event) {
    if ($46c182cf445ced10$var$running) {
        $46c182cf445ced10$var$running = false;
        Tone.Transport.stop();
    } else {
        $46c182cf445ced10$var$running = true;
        Tone.Transport.start();
    // synth.triggerAttackRelease("C4", "8n");
    }
}
function $46c182cf445ced10$var$editInstrument(event) {
    document.querySelector('#song-editor').style.display = 'none';
    document.querySelector('#instrument-editor').style.display = 'block';
}
function $46c182cf445ced10$var$changePadMode(event) {
    let pads = document.querySelector('#pads');
    let samplePads = document.querySelector('#sample-pads');
    if (pads.style.display == 'none') {
        samplePads.style.display = 'none';
        pads.style.display = 'grid';
    } else {
        pads.style.display = 'none';
        samplePads.style.display = 'grid';
    }
}
function $46c182cf445ced10$var$editSong(event) {
    if (document.querySelector('#song-editor').style.display != 'block') {
        let children = document.querySelector('#sequencer').children;
        for (const child of children)if (child.style.display != 'none') child.style.display = 'none';
        document.querySelector('#song-editor').style.display = 'block';
    }
}
function $46c182cf445ced10$var$keyboardEvent(event) {
    var name = event.key;
    var code = event.code;
    if (document.querySelector('#song-editor').style.display == 'block') {
        let pad = '';
        if (code == 'ArrowUp') pad = 'pad7';
        if (code == 'ArrowDown') pad = 'pad11';
        if (code == 'ArrowLeft') pad = 'pad10';
        if (code == 'ArrowRight') pad = 'pad12';
        if (pad != '') $46c182cf445ced10$var$handleSongEditor(pad);
    }
}
function $46c182cf445ced10$var$handleSongEditor(pad) {
    // up, down, left, right
    let activeCellElem = document.querySelector('.active-cell');
    if (pad == 'pad7') {
        let parent = activeCellElem.parentNode;
        let trk = activeCellElem.classList[1];
        let nextActive = parent.previousElementSibling.querySelector('.' + trk);
        nextActive.classList.add('active-cell');
        activeCellElem.classList.remove('active-cell');
    }
    if (pad == 'pad11') {
        let parent = activeCellElem.parentNode;
        let trk = activeCellElem.classList[1];
        let nextActive = parent.nextElementSibling.querySelector('.' + trk);
        nextActive.classList.add('active-cell');
        activeCellElem.classList.remove('active-cell');
    }
    if (pad == 'pad10') {
        let sibling = activeCellElem.previousElementSibling;
        if (sibling.classList.contains('trk')) {
            activeCellElem.classList.remove('active-cell');
            sibling.classList.add('active-cell');
        }
    }
    if (pad == 'pad12') {
        let sibling = activeCellElem.nextElementSibling;
        if (sibling.classList.contains('trk')) {
            activeCellElem.classList.remove('active-cell');
            sibling.classList.add('active-cell');
        }
    }
}
function $46c182cf445ced10$var$triggerPad(event) {
    let pad = event.target.id;
    if (document.querySelector('#song-editor').style.display == 'block') {
        if (pad == 'pad4') {
            document.querySelector('#song-editor').style.display = 'none';
            document.querySelector('#chain-editor').style.display = 'grid';
        } else $46c182cf445ced10$var$handleSongEditor(pad);
    }
}
let $46c182cf445ced10$var$playPad = document.querySelector('#play-pad');
$46c182cf445ced10$var$playPad.addEventListener('click', $46c182cf445ced10$var$startStop);
let $46c182cf445ced10$var$instrPad = document.querySelector('#pad14');
$46c182cf445ced10$var$instrPad.addEventListener('click', $46c182cf445ced10$var$editInstrument);
let $46c182cf445ced10$var$modePad = document.querySelector('#mode-pad');
$46c182cf445ced10$var$modePad.addEventListener('click', $46c182cf445ced10$var$changePadMode);
let $46c182cf445ced10$var$pad7 = document.querySelector('#pad7');
$46c182cf445ced10$var$pad7.addEventListener('click', $46c182cf445ced10$var$triggerPad);
let $46c182cf445ced10$var$pad11 = document.querySelector('#pad11');
$46c182cf445ced10$var$pad11.addEventListener('click', $46c182cf445ced10$var$triggerPad);
let $46c182cf445ced10$var$pad10 = document.querySelector('#pad10');
$46c182cf445ced10$var$pad10.addEventListener('click', $46c182cf445ced10$var$triggerPad);
let $46c182cf445ced10$var$pad12 = document.querySelector('#pad12');
$46c182cf445ced10$var$pad12.addEventListener('click', $46c182cf445ced10$var$triggerPad);
let $46c182cf445ced10$var$editPad = document.querySelector('#edit-pad');
$46c182cf445ced10$var$editPad.addEventListener('click', $46c182cf445ced10$var$triggerPad);
let $46c182cf445ced10$var$songPad = document.querySelector('#song-pad');
$46c182cf445ced10$var$songPad.addEventListener('click', $46c182cf445ced10$var$editSong);
window.addEventListener('keydown', $46c182cf445ced10$var$keyboardEvent, true);


