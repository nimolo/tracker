# Web Audio Tracker

Web audio tracker inspired by the Dirtywave M8, LSDJ, Polyend Tracker, Renoise, and others.


## Getting started

This project use parcel and npm to manage JavaScript resources and deploy assets.

    $ npm install

## Build Release

    $ npx parcel build src/index.html --no-optimize --no-content-hash --no-source-maps --public-url .
