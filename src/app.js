console.log('tracker version 0.1');

let running = false;

class Project {
    constructor() {
    }
}

let tracks = [];
let chains = {
};
let phrases = {
};
let instruments = {
};
let pads = [];
let samples = {
};
let fxs = [];
let initialized = false;

let synth = null;
let synthA = null;
let synthB = null;
let masterReverb = null;


function addInstrumentLFO(instrument, patch) {
}

class SampleInstrument {
    constructor() {
        this.instrument = new Tone.Player().toDestination();
    }
}

class SynthInstrument {
    constructor() {
        this.instrument = new Tone.Synth().toDestination();
    }
}

let initializer = document.querySelector('#initializer');
initializer.addEventListener('click', initApp);

function moveMeasure() {
}

function moveNote() {
}

function initApp(event) {
    if (document.documentElement.requestFullscreen) {
        let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            document.documentElement.requestFullscreen();
        }
        Tone.start().then(() => {
            console.log('audio is ready');
            event.target.removeEventListener('click', initApp);
            event.target.remove();
            document.querySelector('#song-editor').style.display = 'block';
        });
        if (!initialized) {
            initialized = true;
            Tone.Transport.scheduleRepeat((time) => {
                // use the callback time to schedule events
                // osc.start(time).stop(time + 0.1);
                moveNote();
            }, "8n");
            /*
            Tone.Transport.scheduleRepeat((time) => {
                moveMeasure();
            }, "1n");
            */
            masterReverb = new Tone.Reverb({'decay': 2.0, 'wet': 0.25}).toDestination();
            synth = new Tone.Synth().chain(masterReverb); // .toDestination();
            let lfo = new Tone.LFO("4n", 400, 4000).start(0);
            synthA = new Tone.FMSynth(); // .toDestination();
            synthA.chain(masterReverb);
            const part = new Tone.Part(((time, value) => {
                // the value is an object which contains both the note and the velocity
                // console.log(value.instrument);
                let instrument = value.instrument;
                instrument.triggerAttackRelease(value.note, "8n", time, value.velocity);
            }),
            [
                { time: 0, note: "C3", velocity: 0.9, instrument: synth},
                { time: "1:0", note: "C4", velocity: 0.5, instrument: synth},
                { time: "2:0", note: "F4", velocity: 0.5, instrument: synth},
                { time: "3:0", note: "G4", velocity: 0.5, instrument: synth},
            ]
            ).start(0);
            const loopA = new Tone.Loop(time => {
                synthA.triggerAttackRelease("C2", "8n", time);
            }, "4n").start(0);
            let noiseSynth = new Tone.NoiseSynth({'volume': -9}).toDestination();
            const noiseLoop = new Tone.Loop(time => {
                noiseSynth.triggerAttackRelease("16n", time);
            }, "8n").start(0);
        }
    } else {
        alert('no fullscreen');
    }
}

function startStop(event) {
    if (running) {
        running = false;
        Tone.Transport.stop();
    } else {
        running = true;
        Tone.Transport.start();
        // synth.triggerAttackRelease("C4", "8n");
    }
}

function editInstrument(event) {
    document.querySelector('#song-editor').style.display = 'none';
    document.querySelector('#instrument-editor').style.display = 'block';
}

function changePadMode(event) {
    let pads = document.querySelector('#pads');
    let samplePads = document.querySelector('#sample-pads');
    if (pads.style.display == 'none') {
        samplePads.style.display = 'none';
        pads.style.display = 'grid';
    } else {
        pads.style.display = 'none';
        samplePads.style.display = 'grid';
    }
}

function editSong(event) {
    if (document.querySelector('#song-editor').style.display != 'block') {
        let children = document.querySelector('#sequencer').children;
        for (const child of children) {
            if (child.style.display != 'none') {
                child.style.display = 'none';
            }
        }
        document.querySelector('#song-editor').style.display = 'block';
    }
}

function keyboardEvent(event) {
    var name = event.key;
    var code = event.code;
    if (document.querySelector('#song-editor').style.display == 'block') {
        let pad = '';
        if (code == 'ArrowUp') {
            pad = 'pad7';
        }
        if (code == 'ArrowDown') {
            pad = 'pad11';
        }
        if (code == 'ArrowLeft') {
            pad = 'pad10';
        }
        if (code == 'ArrowRight') {
            pad = 'pad12';
        }
        if (pad != '') {
            handleSongEditor(pad);
        }
    }
}

function handleSongEditor(pad) {
    // up, down, left, right
    let activeCellElem = document.querySelector('.active-cell');
    if (pad == 'pad7') {
        let parent = activeCellElem.parentNode;
        let trk = activeCellElem.classList[1];
        let nextActive = parent.previousElementSibling.querySelector('.' + trk);
        nextActive.classList.add('active-cell');
        activeCellElem.classList.remove('active-cell');
    }
    if (pad == 'pad11') {
        let parent = activeCellElem.parentNode;
        let trk = activeCellElem.classList[1];
        let nextActive = parent.nextElementSibling.querySelector('.' + trk);
        nextActive.classList.add('active-cell');
        activeCellElem.classList.remove('active-cell');
    }
    if (pad == 'pad10') {
        let sibling = activeCellElem.previousElementSibling;
        if (sibling.classList.contains('trk')) {
            activeCellElem.classList.remove('active-cell');
            sibling.classList.add('active-cell');
        }
    }
    if (pad == 'pad12') {
        let sibling = activeCellElem.nextElementSibling;
        if (sibling.classList.contains('trk')) {
            activeCellElem.classList.remove('active-cell');
            sibling.classList.add('active-cell');
        }
    }
}

function triggerPad(event) {
    let pad = event.target.id;
    if (document.querySelector('#song-editor').style.display == 'block') {
        if (pad == 'pad4') {
            document.querySelector('#song-editor').style.display = 'none';
            document.querySelector('#chain-editor').style.display = 'grid';
        } else {
            handleSongEditor(pad);
        }
    }
}

let playPad = document.querySelector('#play-pad');
playPad.addEventListener('click', startStop);

let instrPad = document.querySelector('#pad14');
instrPad.addEventListener('click', editInstrument);

let modePad = document.querySelector('#mode-pad');
modePad.addEventListener('click', changePadMode);

let pad7 = document.querySelector('#pad7');
pad7.addEventListener('click', triggerPad);

let pad11 = document.querySelector('#pad11');
pad11.addEventListener('click', triggerPad);

let pad10 = document.querySelector('#pad10');
pad10.addEventListener('click', triggerPad);

let pad12 = document.querySelector('#pad12');
pad12.addEventListener('click', triggerPad);

let editPad = document.querySelector('#edit-pad');
editPad.addEventListener('click', triggerPad);

let songPad = document.querySelector('#song-pad');
songPad.addEventListener('click', editSong);

window.addEventListener('keydown', keyboardEvent, true);
